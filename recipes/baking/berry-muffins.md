# Berry Muffins
-------------------------------------
##### Serves 12 - 5P/25C/6F

### Ingredients
* 50g melted coconut oil 
* 260g kalo berry greek yogurt
* 1/2 cup maple syrup (138 g) <-- this can be substituted for 90g of golden syrup if you don't have maple on hand.
* 2 eggs (106g)
* 1 teaspoon vanilla extract
* zest of 1 lemon
* 125g all purpose flour
* 125g whole wheat flour
* 2 teaspoons baking powder
* 1 teaspoon baking soda
* 1/2 teaspoon salt
* 275g frozen berries - fresh also works.

### Instructions
1. Heat oven to 200°C.
2. In a large bowl, place the melted coconut oil. Stir in the greek yogurt, maple syrup, eggs and vanilla, and stir until no lumps remain. If coconut oil starts to solidify, pop the mixture into the microwave and heat on 20 second increments until it goes smooth again.
3. Stir in the flours, baking powder, baking soda and salt. Mix until completely combined, but be careful not to overmix.
4. Fold in the berries and lemon zest.
5. Divide amongst 12 muffin cups. 
6. Bake for 25 minutes, or until a toothpick comes out cleanly.

## Storage
These muffins can be stored for up to 4 days in a sealed container at room temperature, or in a freezer bag in the freezer for up to 3 months.

This is an adaption of the below recipe:
* https://sweetpeasandsaffron.com/healthy-blueberry-muffins/