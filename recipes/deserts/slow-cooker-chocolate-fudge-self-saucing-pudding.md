# Slow Cooker Self Saucing Chocolate Fudge Pudding
-----------------------------------

Put it on in two hours before you plan on eating dessert and let the aroma of chocolate waft through your home! Serve with vanilla ice cream, cream, and perhaps some fresh custard and berries.

If you double the recipe, cook for 3 hours

* Serves: 3-4
* Prep time: 15 mins
* Cook time: 2 hours

## INGREDIENTS

* 1 cup Flour (Plain)
* 3/4 cup Brown Sugar firmly packed
* 2 tbsp Cocoa
* 2 tsp Baking Powder
* 1/4 tsp Salt

### Liquid Ingredients

* 1/2 cup Milk
* 90g of melted butter
* 1/2 tsp Vanilla Extract

### Chocolate Fudge Sauce Ingredients

* 3/4 cup Brown Sugar firmly packed
* 2 tbsp Cocoa
* 3/4 cup Hot Water

## METHOD

1. Place first group of ingredients together in a bowl and stir well.

2. Add liquid ingredients, stir well and pour into the slow cooker.

3. In a glass jug or bowl, mix together the Chocolate Fudge Sauce Ingredients and carefully pour over the ingredients in the slow cooker.

4. DO NOT STIR.

5. Cover and cook on high for 2 hours or until a metal skewer inserted comes out clean.

## REFERENCES
We modified the below recipe by replacing the oil with butter.

* https://www.stayathomemum.com.au/recipes/slowcooker-chocolate-fudge-pudding/

